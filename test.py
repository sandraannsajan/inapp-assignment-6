import sqlite3
conn = sqlite3.connect('db.sqlite')
cursor = conn.cursor()
print("Question 1b")
output1=cursor.execute("SELECT HomeTeam,AwayTeam FROM Matches  WHERE FTHG==5 and Date like '2015%'")
print(output1.fetchall())

print("\nQuestion 1c")
output2=cursor.execute("SELECT * FROM Matches WHERE FTR=='A' and HomeTeam=='Arsenal'")
print(output2.fetchall())

print("\nQuestion 1d")
output3=cursor.execute("SELECT * FROM Matches WHERE (Season>=2012 and Season<=2015) and AwayTeam=='Bayern Munich' and FTHG>2")
print(output3.fetchall())

print("\nQuestion 1e")
output4=cursor.execute("SELECT * FROM Matches WHERE HomeTeam like 'A%' and AwayTeam like 'M%'")
print(output4.fetchall())

print("\nQuestion 2a")
output5=cursor.execute("SELECT COUNT(*) FROM TEAMS")
print(output5.fetchall())

print("\nQuestion 2b")
output6=cursor.execute("SELECT DISTINCT Season FROM TEAMS")
print(output6.fetchall())

print("\nQuestion 2c")
output7=cursor.execute("SELECT MAX(StadiumCapacity), MIN(StadiumCapacity) FROM TEAMS")
print(output7.fetchall())

print("\nQuestion 2d")
output8=cursor.execute("SELECT SUM(KaderHome) FROM TEAMS WHERE Season==2014")
print(output8.fetchall())

print("\nQuestion 2e")
output9=cursor.execute("SELECT AVG(FTHG) FROM Matches WHERE HomeTeam=='Man United'")
print(output9.fetchall())

print("\nQuestion 3a")
output10=cursor.execute("SELECT HomeTeam, FTHG, FTAG FROM Matches WHERE Season==2010 and HomeTeam='Aachen' ORDER BY FTHG DESC")
print(output10.fetchall())

print("\nQuestion 3b")
output11=cursor.execute("SELECT HomeTeam, COUNT(*) FROM Matches WHERE Season==2016 and FTR=='H' GROUP BY HomeTeam ORDER BY COUNT(*) DESC")
print(output11.fetchall())

print("\nQuestion 3c")
output12=cursor.execute("SELECT * FROM Unique_Teams LIMIT 10")
print(output12.fetchall())

print("\nQuestion 3d(WHERE)")
output13a=cursor.execute("SELECT t1.Match_ID, t1.Unique_Team_ID, t2.TeamName FROM Unique_Teams t2, Teams_in_Matches t1 WHERE t1.Unique_Team_ID=t2.Unique_Team_ID")
print(output13a.fetchall())

print("\nQuestion 3d(JOIN)")
output13b=cursor.execute("SELECT Teams_in_Matches.Match_ID, Teams_in_Matches.Unique_Team_ID, Unique_Teams.TeamName FROM Unique_Teams JOIN Teams_in_Matches ON Teams_in_Matches.Unique_Team_ID=Unique_Teams.Unique_Team_ID")
print(output13b.fetchall())

print("\nQuestion 3e")
output14=cursor.execute("SELECT * FROM Unique_Teams JOIN Teams ON Teams.TeamName=Unique_Teams.TeamName LIMIT 10")
print(output14.fetchall())

print("\nQuestion 3f")
output15=cursor.execute("SELECT Unique_Teams.Unique_Team_ID, Unique_Teams.TeamName, Teams.AvgAgeHome, Teams.Season, Teams.ForeignPlayersHome FROM Unique_Teams JOIN Teams ON Teams.TeamName=Unique_Teams.TeamName LIMIT 5")
print(output15.fetchall())

print("\nQuestion 3g")
output16=cursor.execute("SELECT MAX(Teams_in_Matches.Match_ID), Teams_in_Matches.Unique_Team_ID, Unique_Teams.TeamName FROM Unique_Teams JOIN Teams_in_Matches ON Teams_in_Matches.Unique_Team_ID=Unique_Teams.Unique_Team_ID WHERE Unique_Teams.TeamName LIKE '%y' or Unique_Teams.TeamName LIKE '%r' GROUP BY Unique_Teams.TeamName")
print(output16.fetchall())

conn.commit()
conn.close()
